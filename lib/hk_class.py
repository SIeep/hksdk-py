from ctypes import *

SERIALNO_LEN = 48  # 序列号长度
NAME_LEN = 32  # 用户名长度py

class NET_DVR_DEVICEINFO_V30(Structure):
    _fields_ = [('sSerialNumber', c_ubyte * SERIALNO_LEN), ('byAlarmInPortNum', c_byte), ('byAlarmOutPortNum', c_byte), ('byDiskNum', c_byte),
                ('byDVRType', c_byte), ('byChanNum', c_byte), ('byStartChan', c_byte), ('byAudioChanNum', c_byte), ('byIPChanNum', c_byte), ('byRes1', c_ubyte * 24)]

class NET_VCA_POINT(Structure):
    _fields_ = [('fX', c_float), ('fY', c_float)]

class NET_VCA_POLYGON(Structure):
    _fields_ = [('dwPointNum', c_ulong), ('struPos',NET_VCA_POINT * 10)] 

class NET_DVR_THERMOMETRY_PRESETINFO_PARAM(Structure):
    _fields_ = [('byEnabled', c_byte), ('byRuleID', c_short), ('wDistance', c_short), ('fEmissivity', c_float), ('byDistanceUnit', c_byte), ('byRes', c_ubyte * 2), ('byReflectiveEnabled', c_byte),
                ('fReflectiveTemperature', c_float), ('szRuleName', c_ubyte * NAME_LEN), ('byRes1', c_ubyte * 63), ('byRuleCalibType', c_byte), ('struPoint', NET_VCA_POINT), ('struRegion', NET_VCA_POLYGON)]

class NET_DVR_THERMOMETRY_PRESETINFO(Structure):
    _fields_ = [('dwSize', c_ulong), ('wPresetNo', c_short), ('byRes', c_ubyte * 2),
                ('struPresetInfo', NET_DVR_THERMOMETRY_PRESETINFO_PARAM * 40)]

class NET_DVR_THERMOMETRY_COND(Structure):
    _fields_ = [('dwSize', c_ulong), ('dwChannel', c_ulong),
                ('wPresetNo', c_short), ('byRes', c_ubyte * 62)]

class BYTE_ARRAY(Structure):
    _fields_ = [('byValue', c_byte * 2097152)]

class NET_DVR_STD_CONFIG(Structure):
    _fields_ = [('lpCondBuffer', POINTER(NET_DVR_THERMOMETRY_COND)), ('dwCondSize', c_ulong), ('lpInBuffer', POINTER(NET_DVR_THERMOMETRY_PRESETINFO)), ('dwInSize', c_ulong), ('lpOutBuffer', POINTER(NET_DVR_THERMOMETRY_PRESETINFO)), ('dwOutSize', c_ulong),
                ('lpStatusBuffer', POINTER(BYTE_ARRAY)), ('dwStatusSize', c_ulong), ('lpXmlBuffer', c_void_p), ('dwXmlSize', c_ulong), ('byDataType', c_bool), ('byRes', c_ubyte * 23)]

class NET_VCA_RECT(Structure):
    _fields_ = [('fX', c_char),('fY', c_char),('fWidth', c_char),('fHeight', c_char)]

class NET_DVR_JPEGPICTURE_WITH_APPENDDATA(Structure):
    _fields_ = [('dwSize', c_int32), ('dwChannel', c_int32), ('dwJpegPicLen', c_int32), ('pJpegPicBuff', POINTER(BYTE_ARRAY)), ('dwJpegPicWidth', c_int32),
                ('dwJpegPicHeight', c_int32), ('dwP2PDataLen', c_int32), ('pP2PDataBuff', POINTER(BYTE_ARRAY)), ('byIsFreezedata', c_byte), ('byRes', c_byte * 255)]

# JPEG图像信息结构体。

# struct{
#   WORD     wPicSize;
#   WORD     wPicQuality;
# }NET_DVR_JPEGPARA,*LPNET_DVR_JPEGPARA;

class NET_DVR_JPEGPARA(Structure):
    _fields_ = [('wPicSize', c_ulong),('wPicQuality', c_ulong)]
