import hk_sdk as hk_sdk
import lib.hk_class as hkclass
import time

from multiprocessing import Process

'''
lUserID 登录sdk返回的id 
jpg_name 文件保存名 
'''
def get_webcam_picture(hk_sdk, lUserID, jpg_name, isRun=True):
    while isRun:
        result = hk_sdk.captureJPEGPicture(lUserID, 1, bytes(jpg_name.encode('utf-8')))
        print("result ", result, lUserID)

if __name__ == "__main__":

    #sdk初始化
    result = hk_sdk.init()

    #存放登录的sdk集合
    login_webcam_process = []
    
    if result:
        print('初始化成功')

        #登录sdk
        lUserID = hk_sdk.login(b"192.168.8.15", 8000, b'admin', b'a1234567')
        if lUserID != -1:
            login_webcam_process.append({'userId':lUserID, 'isRun' : True, 'process' : None})

        #开启进程抓拍图片
        for j in login_webcam_process:
            #保存的图片
            jpg_name = './sdklog/t' + str(j) + '.jpg'

            j['process'] = Process(target=get_webcam_picture, args=(hk_sdk, j['userId'], jpg_name, j['isRun']))
            j['process'].start()
            

        time.sleep(20)

        # 退出登录
        for j in login_webcam_process:
            hk_sdk.logout(j['userId'])
            j['isRun'] = False
            j['process'].terminate()
        
        #释放sdk
        hk_sdk.cleanup()
            
